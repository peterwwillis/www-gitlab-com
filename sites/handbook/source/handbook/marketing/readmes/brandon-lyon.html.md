## Brandon Lyon's README

Hi 👋 my name is Brandon Lyon. I'm a marketing frontend engineer and UX.

- [GitLab Profile](https://gitlab.com/brandon_lyon)
- [My Website](http://about.brandonmlyon.com/)
- [Twitter](https://twitter.com/brandon_m_lyon)
- [Pinterest](https://www.pinterest.com/designbybrandon/)
- [LinkedIn](https://www.linkedin.com/in/brandonmlyon/)

## About Me

Hybrid designer-developers are rare. I'm a self-taught developer since before CSS existed. I went to college to become an architect but also studied graphic design. I spend my free time reading professional journals to keep up to date on industry trends.

- ❤️ I love design and development.
- 🏄 I've travelled the world but California is the only place that feels like home.
- 📖 I value knowledge.
- 🍷 Coffee, tea, or wine please.
- 🕹️ Video games are my primary recreation.
- 🥏 Sports aren't my thing but I do play frisbee golf.
- 🌲🐈 I love plants and animals.

### Myers-Briggs Personality type:

🔗[**“The Logician”** (INTP)](https://www.16personalities.com/intp-personality)

### 10 Books On My Bookshelf

01. Are Your Lights On?
02. Graphic Content: True Stories From Top Creatives
03. Humans vs Computers
04. The Oxford Companion to American Food and Drink
05. The Architecture of Happiness
06. When: The Scientific Secrets of Perfect Timing
07. 101 Things I Learned In Architecture School
08. Tao of Jeet Kun Do
09. Elements of Persuasion
10. Blood, Sweat, and Pixels

## How I think

- I operate in a judgement-free zone.
- I want you to feel comfortable around me. Please let me know if there is any way I can improve.
- The phrase "intent vs impact" is of high importance to me.
- I prefer the frank and direct approach and won't take it personally. Decades of design reviews have hardened me.
- I understand and empathize that others might not like the direct approach.
- I value self-sufficiency.
- I prefer long term thinking over short term thinking.
- I honor the 5 Ws. Who, what, when, where, and why? I love it when you ask those questions too.
- I am better at planning than improvising.
- Entropy & chaos will happen. I try to expect the unexpected.
- I'm somewhere in-between introvert and extrovert. Interacting with people can be exhausting but it's also enjoyable.
- Please help me document things! I have a good short term memory but a terrible long term memory.
- My background in architecture adds a unique behavioral psychology element to my designs.

## Working with me

First off, I'm in the Pacific time zone and I'm not a morning person. That said, I'm happy to be accommodate your schedule. We're all in this together!

### Planning

There are only so many hours in a week. If you add something to my sprint, then something else has to be removed. Sprints & milestones are used to make plans. Lead time is usually measured in weeks and months, not hours. I understand that it's important to be flexible and pivot when appropriate but it's also difficult to deliver and stressful when that happens too often.

### Communication

I try to limit my project planning to one day per week. That focus helps me accomplish any planned tasks. If you need to contact me urgently then you might want to try Slack. That said, I tend to turn off Slack during the second half of the day. This also helps me focus on completing planned tasks.

## Favorite quotes

> If a park ranger warns you about the bears, it ain’t cause he’s trying to keep all the bear hugs for himself.

> A designer knows he has achieved perfection not when there is nothing left to add, but when there is nothing left to take away.

> If you don't have time to do it right, when will you have time to do it over?

> Everybody has a plan until they get punched in the face.

> I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel.

## Conclusion

🤘 Be excellent to each other.
