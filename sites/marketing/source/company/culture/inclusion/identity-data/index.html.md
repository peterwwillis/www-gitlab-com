---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2020-11-30

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 139       | 11.01%          |
| Based in EMEA                               | 343       | 27.18%          |
| Based in LATAM                              | 21        | 1.66%           |
| Based in NORAM                              | 759       | 60.14%          |
| **Total Team Members**                      | **1,262** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 870       | 68.94%          |
| Women                                       | 392       | 31.06%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,262** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 158       | 67.81%          |
| Women in Management                         | 75        | 32.19%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **233**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 73        | 73.74%          |
| Women in Leadership                         | 26        | 26.26%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **99**    | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 434       | 80.07%          |
| Women in Engineering                        | 108       | 19.93%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **542**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.14%           |
| Asian                                       | 46        | 6.44%           |
| Black or African American                   | 17        | 2.38%           |
| Hispanic or Latino                          | 38        | 5.32%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 32        | 4.48%           |
| White                                       | 438       | 61.34%          |
| Unreported                                  | 142       | 19.89%          |
| **Total Team Members**                      | **714**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 17        | 8.21%           |
| Black or African American                   | 3         | 1.45%           |
| Hispanic or Latino                          | 7         | 3.38%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.35%           |
| White                                       | 137       | 66.18%          |
| Unreported                                  | 34        | 16.43%          |
| **Total Team Members**                      | **207**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 8         | 5.41%           |
| Black or African American                   | 3         | 2.03%           |
| Hispanic or Latino                          | 5         | 3.38%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 4.73%           |
| White                                       | 92        | 62.16%          |
| Unreported                                  | 33        | 22.30%          |
| **Total Team Members**                      | **148**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 8         | 10.26%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 5.13%           |
| White                                       | 49        | 62.82%          |
| Unreported                                  | 17        | 21.79%          |
| **Total Team Members**                      | **78**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.08%           |
| Asian                                       | 118       | 9.35%           |
| Black or African American                   | 29        | 2.30%           |
| Hispanic or Latino                          | 66        | 5.23%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.08%           |
| Two or More Races                           | 40        | 3.17%           |
| White                                       | 709       | 56.18%          |
| Unreported                                  | 298       | 23.61%          |
| **Total Team Members**                      | **1,262** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 59        | 10.89%          |
| Black or African American                   | 9         | 1.66%           |
| Hispanic or Latino                          | 26        | 4.80%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.18%           |
| Two or More Races                           | 15        | 2.77%           |
| White                                       | 306       | 56.46%          |
| Unreported                                  | 126       | 23.25%          |
| **Total Team Members**                      | **542**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 15        | 6.44%           |
| Black or African American                   | 3         | 1.29%           |
| Hispanic or Latino                          | 7         | 3.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 8         | 3.43%           |
| White                                       | 145       | 62.23%          |
| Unreported                                  | 55        | 23.61%          |
| **Total Team Members**                      | **233**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 9.09%           |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.01%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 4.04%           |
| White                                       | 59        | 59.60%          |
| Unreported                                  | 26        | 26.26%          |
| **Total Team Members**                      | **99**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 13        | 1.03%           |
| 25-29                                       | 205       | 16.24%          |
| 30-34                                       | 355       | 28.13%          |
| 35-39                                       | 284       | 22.50%          |
| 40-49                                       | 276       | 21.87%          |
| 50-59                                       | 113       | 8.95%           |
| 60+                                         | 16        | 1.27%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,262** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
