---
layout: job_family_page
title: Analytics and Insights
---
The Analytics and Insights job family is responsible for providing data driven insights to the business to improve decision making. These are embedded analytics teams that live in the functional areas and provide data requirements to the centralized data team.
 
## Responsibilities
- Business Partnership: Own one stakeholder relationships regarding either business strategy, corporate metric forecasting, go to market, product strategy, pricing strategy and functional strategy needed to answer GitLab’s hardest questions.
- Data Subject Matter Expert.
- Partner with executive team members across the company to provide data driven decision support using your analytical, business and financial acumen.
- Highly skilled in being able to provide analysis and scientific data to answer GitLab’s toughest analytical questions.
- Financial Modeling: Build bottoms up decision support models to measure and drive key decisions in the business.
- Communication: Prepare and review visualizations of all types of data to promote internal understanding of your team's insights. Expert influencer to our executive team. Clearly articulate insights.
- Data Science Modeling: Design, implement and apply analytics techniques (e.g., optimization, machine learning, experimentation, mathematical modeling) using analytical tools and programming languages (e.g., R, Python, SAS). Be comfortable with a boring solution such as a heuristic when it helps drive the insight.
- Data Driven Insights and Analysis: Summarize key data driven insights to members of the executive team to drive better outcomes, an increase in revenue or decrease in cost. Provide insights across the company.
- The ability to discover data gaps and provide requirements to the Data team and to product management for the proper logging and data repositories.
- Collaborate closely with the executive team, product leadership, and our Data team.
- Support the Product function with revenue insight analysis for growth and pricing teams.
- Support the Finance function with corporate metric forecasting and driver analysis.
- Be a culture definer and evolver of GitLab Values.
 
## Requirements
- Data Analysis: A passion for understanding business questions and making data driven insights. Excellent analytical skills.
- Proficient in SQL; R, Python preferred.
- Financial Modeling: Be able to create financial models that follow industry best practice.  Expertise in Google sheets (we do not use excel for modeling purposes).
- A/B Testing experience: experience designing and executing A/B tests in collaboration with growth or product teams.
- Business Acumen: Be able to understand the business at a level to influence EVP priorities and company strategy.
- Business Partnership: Consistent track record of using quantitative analysis to impact key business decisions.
- Communication: Ability to present financial data concisely through written and oral communication. Expert at influencing business stakeholders.
- BS degree in Mathematics, Engineering, Statistics or relevant degree. MBA preferred.
- Ability to use GitLab.
 
## Levels
### Analytics and Insights Senior Analyst
The Analytics and Insights Senior Analyst reports to the Director, Business Analytics and Insights.
 
#### Job Grade
The Analytics and Insights Senior Analyst is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Responsibilities
- Data Subject Matter Expert: Identify data gaps and document it for our product, data and IT teams. Improve your personal data workflow.
- Testing and test design: Execute A/B tests, experiments that affect user behavior using the existing frameworks.
 
#### Requirements
- 5-7 years of experience in a consulting or analytics role ideally with enterprise SaaS software model.
 
#### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to a 50 minute interview with the Business Analytics and Insights Director.
- Then, candidates will be invited to schedule four separate 40 minute interviews; one with the senior member of the data Team, one with the Business Analytics and Insights Manager and two with two separate key business stakeholders in the business (Director or above).
- Final candidates will be invited to conduct a 50 minute interview with the VP Finance.
 
### Business Analytics and Insights Manager
The Business Analytics and Insights Manager reports to the Director, Business Analytics and Insights.

#### Job Grade
The Business Analytics and Insights Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Responsibilities
- Data Subject Matter Expert: Identify and drive improvements to processes and policies in collaboration with product, data and IT teams on process improvements on data collection or data instrumentation.
- Testing and test design: Be a leader in designing A/B tests, experiments that affect user behavior.
 
#### Requirements
- Experience recommendation: 7-10 years of experience in a consulting or analytics role ideally with enterprise SaaS software model.
 
#### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to a 50 minute interview with the Business Analytics and Insights Director.
- Then, candidates will be invited to schedule four separate interviews; one with the Director of Data for 40 minutes, one with the Director, FP&A and Manager FP&A for 50 minutes and two with two separate key business stakeholders in the business (Director or above) for 40 minutes.
- Final candidates will be invited to conduct two 50 minute interviews, with the VP Finance and with the CFO.
 
### Director, Business Analytics and Insights
The Director, Business Analytics and Insights reports to the VP of Finance.

#### Job Grade
The Director, Business Analytics and Insights is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Responsibilities
- Participate in monthly key reviews. Provide insights across the company starting with a focus on product and website analytics, customer behavior including the pricing and growth teams.
- Data Subject Matter Expert: Identify and drive improvements to processes and policies in collaboration with product, data and IT teams on process improvements on data collection or data instrumentation.
- Corporate Metrics forecasting: Business owner of corporate metrics reporting and forecasting. - Identify and ensure business definitions are accurate and the processes for reporting are reliable.
- Testing and test design: A leader in designing A/B tests, experiments that affect user behavior and broaden these tests into an A/B testing framework for the company.
- Management: Set the vision, hire, build, coach and manage a highly productive team day to day. Expert management skills, especially at developing talent. Set goals and priorities for the team.
- Review: Review work and analysis produced by the team you manage or from other members of the team. Expert at providing feedback.
 
#### Requirements
- Management: 5-7 years of management experience in a consulting, analytics or data science role.
- Experience recommendation: 12-15 years of experience in a consulting or analytics role ideally with enterprise SaaS software model.

#### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to a 50 minute interview with the VP of Finance.
- Then, candidates will be invited to schedule four separate interviews; one with the Director of Data for 40 minutes, one with the Director, FP&A and Manager FP&A for 50 minutes, one with the Director, Pricing for 40 minutes and one with the Director, Growth for 40 minutes. 
- Final candidates will be invited to conduct two-three 40 minute interviews, with one or two e-Group team member(s) and lastly with the CFO.
 
Additional details about our process can be found on our [hiring page](/handbook/hiring).
 
## Career Ladder
The next step in the Analytics and Insights job family is to move to a senior leader or executive leadership job family of which we do not yet have defined at GitLab.

## Performance Indicators
- Throughput: [Issues or merge requests closed as measurement of analyses completed](/handbook/ceo/chief-of-staff-team/performance-indicators/#throughput-for-the-cost)
- Business partnering satisfaction
- Dollars of ARR growth found through Analysis
